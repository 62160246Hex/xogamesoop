package com.hex.xogamesoop;
import java.util.Scanner;

public class Game {
    
    static Scanner getInput = new Scanner(System.in);
    
    static void writeLine(String value){
        System.out.println(value);
    }

    static void showWelcome(int tableId){
        writeLine("Welcome to XO Games [ Board."+tableId+" ]");
    }

    static void runTable(Table table){

        showWelcome(table.getId());

        do{

            table.showTable();
            table.showTurn();
            writeLine("Please input Row Col: ");
            table.insertXO( getInput.nextInt(),  getInput.nextInt());
            
        }while( !table.isDone() );

    }

    public static void main(String args[]) {

        Player playerHex = new Player("Hex.Jakkrit"),
                playerAFU = new Player("AFU.Xhao");
        
        Table tableA = new Table(playerHex, playerAFU);
        
        runTable(tableA);

    }

}

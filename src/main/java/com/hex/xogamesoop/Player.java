package com.hex.xogamesoop;

public class Player {

    private String name;
    
    private int win, lose, draw;

    public Player(String name) {
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getWin(){
        return this.win;
    }

    public void win(){
        this.win++;
    }
    
    public int getLose(){
        return this.lose;
    }

    public void lose(){
        this.lose++;
    }

    public int getDraw(){
        return this.draw;
    }

    public void draw(){
        this.draw++;
    }

}

package com.hex.xogamesoop;

import java.util.Scanner;

public class Table {
    public static int tableDone = 0;
    private Scanner getInput = new Scanner(System.in);
    private int[] table;
    private Player playerX, playerO, currentPlayer;
    private int round, finished, id;

    public Table(Player playerX, Player playerO){
        this.playerX = playerX;
        this.playerO = playerO;
        this.currentPlayer = this.playerX;
        this.round = 1;
        this.id = ( tableDone + 1 );
        this.finished = -1;
        this.table = new int[]{
            0, 0, 0,
            0, 0, 0,
            0, 0, 0
        };
    }

    private void resetDeafult(){

        this.currentPlayer = this.playerX;
        this.round = 1;
        this.id = ( tableDone + 1 );
        this.finished = -1;
        this.table = new int[]{
            0, 0, 0,
            0, 0, 0,
            0, 0, 0
        };

    }

    private void writeLine(String value){
        System.out.println(value);
    }

    private boolean iscorrectInsert(int row, int col){

        int rowSelected = row * 3 - 1;
        if (rowSelected > 8 || rowSelected < 2 || col < 1 || col > 3) {
            writeLine("\n\n[ !Error ] OX Position input wrong...\n");
            return false;
        }

        int posSecelect = (rowSelected - 2) + (col - 1);
        if (this.table[posSecelect] != 0) {
            writeLine("\n\n[ !Error ] Position already assiged before...\n");
            return false;
        }

        return true;
    } 
    
    private void changeTurn(){
        this.currentPlayer = this.currentPlayer == playerO ? playerX : playerO; 
    }

    private void showBye(){
        writeLine("Bye bye ....");
    }

    private void showResult(){
        this.showTable();
        writeLine(this.finished == 3 ? "Draw Game :D [" + this.playerO.getName() + " | " + this.playerX.getName() + "]" : "Player( " + (this.finished % 2 == 1 ? "X : " + this.playerX.getName() : "O : " + this.playerO.getName() ) + " ) Win....");
    }

    public int getWinner(){
        if (this.round < 3) {
            return -1;
        }

        int playerTurn = this.round % 2 == 0 ? 2 : 1;
        
        this.finished = (this.table[0] == playerTurn && this.table[1] == playerTurn && this.table[2] == playerTurn) ? playerTurn :
        (this.table[3] == playerTurn && this.table[4] == playerTurn && this.table[5] == playerTurn) ? playerTurn :
        (this.table[6] == playerTurn && this.table[7] == playerTurn && this.table[8] == playerTurn) ? playerTurn :
        (this.table[0] == playerTurn && this.table[3] == playerTurn && this.table[6] == playerTurn) ? playerTurn :
        (this.table[1] == playerTurn && this.table[4] == playerTurn && this.table[7] == playerTurn) ? playerTurn :
        (this.table[2] == playerTurn && this.table[5] == playerTurn && this.table[8] == playerTurn) ? playerTurn :
        (this.table[0] == playerTurn && this.table[4] == playerTurn && this.table[8] == playerTurn) ? playerTurn :
        (this.table[2] == playerTurn && this.table[4] == playerTurn && this.table[6] == playerTurn) ? playerTurn : round > 9 ? 3 : -1;
        writeLine("this.finished: " + this.finished);
        writeLine("playerTurn: " + playerTurn);
        
        if( this.finished == -1 ){  return this.finished; }
        
        if( this.finished == 1 ) {
            this.playerX.win();
            this.playerO.lose();
        }else if( this.finished == 2 ) {
            this.playerO.win();
            this.playerX.lose();
        }else if( this.finished == 3 ) {
            this.playerO.draw();
            this.playerX.draw();
        }
        return this.finished;
    }

    public void showTable(){
        String tableInfo = "\n  1 2 3";
        int tableRow = 1;

        for (int i = 0; i < this.table.length; i++) {

            if ( i % 3 == 0 ) {
                tableInfo += "\n" + tableRow + " ";
                tableRow++;
            }

            tableInfo += (this.table[i] == 1 ? 'X' : this.table[i] == 2 ? 'O' : '-') + " ";
        }

        writeLine(tableInfo);
    }


    public void insertXO(int row, int col){
        if ( !iscorrectInsert(row, col) ){
            return;
        }

        int position = (row * 3 - 1 - 2) + (col - 1);

        this.table[ position ] = this.round % 2 == 1 ? 1 : 2;
        this.getWinner();
        this.changeTurn();
        this.round++;
    }

	public void testInsertTable(int row, int col){
        if ( !iscorrectInsert(row, col) ){
            return;
        }

        int position = (row * 3 - 1 - 2) + (col - 1);

        this.table[ position ] = this.round % 2 == 1 ? 1 : 2;
        this.round++;
    }

    public void showTurn(){
        writeLine( (this.round % 2 == 0 ? 'O' : 'X') + " Turn | Player( "+ this.currentPlayer.getName() +" )");
    }

    public int getId(){
        return this.id;
    }
    
    private boolean isPlayMoreGames(){
        String playersDecided = "";
        while ( true ){
            writeLine("Want to playe more game ( Y:y / N:n ) ?:");
            playersDecided = this.getInput.next().toUpperCase();
            if (playersDecided.equals("Y")){
                this.resetDeafult();
                return false;
            }else{
                writeLine("Player( " + this.playerO.getName() +" ) \n\tWin: " + this.playerO.getWin() + "\n\tLose: " + this.playerO.getLose() + "\n\tDraw: " + this.playerO.getDraw());
                writeLine("Player( " + this.playerX.getName() +" ) \n\tWin: " + this.playerX.getWin() + "\n\tLose: " + this.playerX.getLose() + "\n\tDraw: " + this.playerX.getDraw());
                this.showBye();
                return true;
            }
        }
    }

    public boolean isDone(){
        if( this.finished == -1) {
            return false;   
        }
        tableDone++;
        this.showResult();

        return true;
    }

}

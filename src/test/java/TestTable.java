/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.hex.xogamesoop.Player;
import com.hex.xogamesoop.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Code.Addict
 */
public class TestTable {
	
	public TestTable() {
	}
	
	@BeforeAll
	public static void setUpClass() {
	}
	
	@AfterAll
	public static void tearDownClass() {
	}
	
	@BeforeEach
	public void setUp() {
	}
	
	@AfterEach
	public void tearDown() {
	}

	// TODO add test methods here.
	// The methods must be annotated with annotation @Test. For example:
	//
	// @Test
	// public void hello() {}
	public void row1ByX(){
        Player xPlayer = new Player("Player1.X");
        Player oPlayer= new Player("Player2.O");
        Table testTable = new Table(xPlayer, oPlayer);
        testTable.testInsertTable(1, 1);
        testTable.testInsertTable(1, 2);
        testTable.testInsertTable(1, 3);
        assertEquals(true, testTable.isDone());
	    assertEquals(true, 1);
    }
	
	public void row2ByX(){
        Player xPlayer = new Player("Player1.X");
        Player oPlayer= new Player("Player2.O");
        Table testTable = new Table(xPlayer, oPlayer);
        testTable.testInsertTable(2, 1);
        testTable.testInsertTable(2, 2);
        testTable.testInsertTable(2, 3);
        assertEquals(true, testTable.isDone());
	    assertEquals(true, testTable.getWinner());
    }

	public void row3ByX(){
        Player xPlayer = new Player("Player1.X");
        Player oPlayer= new Player("Player2.O");
        Table testTable = new Table(xPlayer, oPlayer);
        testTable.testInsertTable(3, 1);
        testTable.testInsertTable(3, 2);
        testTable.testInsertTable(3, 3);
        assertEquals(true, testTable.isDone());
	    assertEquals(true, testTable.getWinner());
    }

	public void col1ByX(){
        Player xPlayer = new Player("Player1.X");
        Player oPlayer= new Player("Player2.O");
        Table testTable = new Table(xPlayer, oPlayer);
        testTable.testInsertTable(1, 1);
        testTable.testInsertTable(2, 1);
        testTable.testInsertTable(3, 1);
        assertEquals(true, testTable.isDone());
	    assertEquals(true, testTable.getWinner());
    }

	public void col2ByX(){
        Player xPlayer = new Player("Player1.X");
        Player oPlayer= new Player("Player2.O");
        Table testTable = new Table(xPlayer, oPlayer);
        testTable.testInsertTable(1, 2);
        testTable.testInsertTable(2, 2);
        testTable.testInsertTable(3, 2);
        assertEquals(true, testTable.isDone());
	    assertEquals(true, testTable.getWinner());
    }

	public void col3ByX(){
        Player xPlayer = new Player("Player1.X");
        Player oPlayer= new Player("Player2.O");
        Table testTable = new Table(xPlayer, oPlayer);
        testTable.testInsertTable(1, 3);
        testTable.testInsertTable(2, 3);
        testTable.testInsertTable(3, 3);
        assertEquals(true, testTable.isDone());
	    assertEquals(true, testTable.getWinner());
    }

	public void crossRightByX(){
        Player xPlayer = new Player("Player1.X");
        Player oPlayer= new Player("Player2.O");
        Table testTable = new Table(xPlayer, oPlayer);
        testTable.testInsertTable(1, 3);
        testTable.testInsertTable(2, 2);
        testTable.testInsertTable(3, 1);
        assertEquals(true, testTable.isDone());
	    assertEquals(true, testTable.getWinner());
    }

	public void crossLeftByX(){
        Player xPlayer = new Player("Player1.X");
        Player oPlayer= new Player("Player2.O");
        Table testTable = new Table(xPlayer, oPlayer);
        testTable.testInsertTable(1, 1);
        testTable.testInsertTable(2, 2);
        testTable.testInsertTable(3, 3);
        assertEquals(true, testTable.isDone());
	    assertEquals(true, testTable.getWinner());
    }
}
